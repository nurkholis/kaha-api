package kaha.api.dao;

import java.util.List;

import kaha.api.models.AreaDto;

public interface AreaDao {
	List<AreaDto> searchArea(String key);
	AreaDto getAreaById(int id);
}
