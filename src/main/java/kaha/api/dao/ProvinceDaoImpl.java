package kaha.api.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import kaha.api.mappers.ProvinceRowMapper;
import kaha.api.models.ProvinceDto;

@Repository
public class ProvinceDaoImpl implements ProvinceDao{
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<ProvinceDto> searchProvince(String key) {
		final String sql = "SELECT ID, NAME FROM XREF_PROVINCE WHERE lower(NAME) LIKE ?";
		RowMapper<ProvinceDto> rowMapper = new ProvinceRowMapper();
		return this.jdbcTemplate.query(sql, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, "%"+key+"%");
			}
		}, rowMapper);
	}

	@Override
	public ProvinceDto getProvinceById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
