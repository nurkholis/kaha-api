package kaha.api.dao;

import java.util.List;

import kaha.api.models.ProvinceDto;

public interface ProvinceDao {
	List<ProvinceDto> searchProvince(String key);
	ProvinceDto getProvinceById(int id);
}
