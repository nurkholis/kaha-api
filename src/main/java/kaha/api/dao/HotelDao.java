package kaha.api.dao;

import java.util.List;

import kaha.api.models.HotelDto;

public interface HotelDao {
	List<HotelDto> searchHotel(String key);
	HotelDto getHotelById(int id);
}
