package kaha.api.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import kaha.api.mappers.CityRowMapper;
import kaha.api.models.CityDto;

@Repository
public class CityDaoImpl implements CityDao{
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<CityDto> searchCity(String key) {
		final String sql = "SELECT ID, NAME FROM XREF_CITY WHERE lower(NAME) LIKE ?";
		RowMapper<CityDto> rowMapper = new CityRowMapper();
		return this.jdbcTemplate.query(sql, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, "%"+key+"%");
			}
		}, rowMapper);
	}

	@Override
	public CityDto getCityById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
