package kaha.api.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import kaha.api.mappers.AreaRowMapper;
import kaha.api.models.AreaDto;

@Repository
public class AreaDaoImpl implements AreaDao{
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<AreaDto> searchArea(String key) {
		final String sql = "SELECT ID, NAME FROM XREF_AREA WHERE lower(NAME) LIKE ?";
		RowMapper<AreaDto> rowMapper = new AreaRowMapper();
		return this.jdbcTemplate.query(sql, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, "%"+key+"%");
			}
		}, rowMapper);
	}

	@Override
	public AreaDto getAreaById(int id) {
		// TODO Auto-generated method stub
		return null;
	}


}
