package kaha.api.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import kaha.api.mappers.HotelRowMapper;
import kaha.api.models.HotelDto;


@Repository
public class HotelDaoImpl implements HotelDao{
	@Autowired
    private JdbcTemplate jdbcTemplate;

	@Override
	public List<HotelDto> searchHotel(String key) {
		final String sql = "SELECT ID, NAME FROM HOTEL WHERE lower(NAME) LIKE ?";
		RowMapper<HotelDto> rowMapper = new HotelRowMapper();
		return this.jdbcTemplate.query(sql, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, "%"+key+"%");
			}
		}, rowMapper);
	}

	@Override
	public HotelDto getHotelById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
