package kaha.api.dao;

import java.util.List;

import kaha.api.models.CityDto;

public interface CityDao {
	List<CityDto> searchCity(String key);
	CityDto getCityById(int id);
}
