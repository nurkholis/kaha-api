package kaha.api.controllers;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kaha.api.models.AutocompleteResponseDto;

@Api(value = "Search",  tags="Search", description = "The Kaha search APIs provide you with access to live rates & availability for all kaha properties globally.")
@RequestMapping(SearchController.BASE_URL)
public interface SearchController {
	public static final String BASE_URL = "/1.0/search";
	
	@Cacheable(value = "get-auto", key = "#key")
	@ApiOperation(value = "Search area, properties, and point of interest", notes = "Returns areas, hotels, and POI’s for specified properties.",  tags={ "Search", })
	@GetMapping({"/autocomplete/{key}"})
	@ResponseStatus(HttpStatus.OK)
	AutocompleteResponseDto searchHotels(@PathVariable String key);
	
}
