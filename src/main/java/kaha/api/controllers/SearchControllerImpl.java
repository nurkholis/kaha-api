package kaha.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import kaha.api.models.AutocompleteResponseDto;
import kaha.api.models.HotelDto;
import kaha.api.models.LocationDto;
import kaha.api.service.HotelService;
import kaha.api.service.LocationService;

@RestController
public class SearchControllerImpl implements SearchController{
	@Autowired
	private HotelService hotelService;
	@Autowired
	private LocationService locationService;
	
	@Override
	public AutocompleteResponseDto searchHotels(@PathVariable String key) {
		List<HotelDto> hotels = hotelService.searchHotels(key.toLowerCase());
		LocationDto locations = locationService.searchLocations(key.toLowerCase());
		AutocompleteResponseDto autocomplete = new AutocompleteResponseDto(hotels, locations);
		
		return autocomplete;
	}
	
}