package kaha.api.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import kaha.api.models.AreaDto;

public class AreaRowMapper implements RowMapper<AreaDto>{
	
	@Override
	public AreaDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		AreaDto area = new AreaDto();
		area.setId(rs.getInt("ID"));
		area.setName(rs.getString("NAME"));
		return area;
	}
}
