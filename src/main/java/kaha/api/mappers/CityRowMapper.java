package kaha.api.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import kaha.api.models.CityDto;

public class CityRowMapper implements RowMapper<CityDto>{
	
	@Override
	public CityDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		CityDto city = new CityDto();
		city.setId(rs.getInt("ID"));
		city.setName(rs.getString("NAME"));
		return city;
	}
}
