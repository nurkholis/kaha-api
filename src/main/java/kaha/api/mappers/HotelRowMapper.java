package kaha.api.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import kaha.api.models.HotelDto;

public class HotelRowMapper implements RowMapper<HotelDto>{

	@Override
	public HotelDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		HotelDto hotel = new HotelDto();
		hotel.setId(rs.getInt("ID"));
		hotel.setName(rs.getString("NAME"));
		return hotel;
	}

}
