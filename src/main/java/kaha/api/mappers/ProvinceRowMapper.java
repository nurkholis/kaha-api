package kaha.api.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import kaha.api.models.ProvinceDto;

public class ProvinceRowMapper implements RowMapper<ProvinceDto>{
	
	@Override
	public ProvinceDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProvinceDto province = new ProvinceDto();
		province.setId(rs.getInt("ID"));
		province.setName(rs.getString("NAME"));
		return province;
	}
}
