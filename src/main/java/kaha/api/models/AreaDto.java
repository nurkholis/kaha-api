package kaha.api.models;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="Area")
public class AreaDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2507931481588285168L;
	private Integer id;
	private String  name;
}
