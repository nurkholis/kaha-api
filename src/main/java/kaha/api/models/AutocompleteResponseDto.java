package kaha.api.models;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="AutocompleteResponse")
public class AutocompleteResponseDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -521497062987349505L;
	private List<HotelDto> hotels;
	private LocationDto locations;
}
