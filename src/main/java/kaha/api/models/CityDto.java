package kaha.api.models;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="City")
public class CityDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4274137604591768976L;
	private Integer id;
	private String  name;
}
