package kaha.api.models;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="Hotel")
public class HotelDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 783552622988119341L;
	private Integer id;
	private String  name;
}
