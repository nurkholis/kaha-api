package kaha.api.models;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="Location")
public class LocationDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7323749629275300034L;
	private List<ProvinceDto> provinces;
	private List<CityDto> cities;
	private List<AreaDto> areas;
}
