package kaha.api.models;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="Province")
public class ProvinceDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3461390562438364927L;
	private Integer id;
	private String  name;
}
