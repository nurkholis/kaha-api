package kaha.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kaha.api.dao.HotelDao;
import kaha.api.models.HotelDto;

@Service
public class HotelServiceImpl implements HotelService{
	@Autowired
	private HotelDao hotelDao;

	@Override
	public List<HotelDto> searchHotels(String key) {
		return hotelDao.searchHotel(key);
	}

	@Override
	public HotelDto getHotelById(int id) {
		HotelDto obj = hotelDao.getHotelById(id);
		return obj;
	}

}
