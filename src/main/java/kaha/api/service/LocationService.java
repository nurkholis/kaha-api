package kaha.api.service;

import kaha.api.models.LocationDto;

public interface LocationService {
	LocationDto searchLocations(String key);
}
