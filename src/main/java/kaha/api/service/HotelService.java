package kaha.api.service;

import java.util.List;

import kaha.api.models.HotelDto;

public interface HotelService {
	List<HotelDto> searchHotels(String key);
	HotelDto getHotelById(int id);
}
