package kaha.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kaha.api.dao.AreaDao;
import kaha.api.dao.CityDao;
import kaha.api.dao.ProvinceDao;
import kaha.api.models.AreaDto;
import kaha.api.models.CityDto;
import kaha.api.models.LocationDto;
import kaha.api.models.ProvinceDto;

@Service
public class LocationServiceImpl implements LocationService{
	@Autowired
	private ProvinceDao provinceDao;
	@Autowired
	private CityDao cityDao;
	@Autowired
	private AreaDao areaDao;

	@Override
	public LocationDto searchLocations(String key) {
		List<ProvinceDto> provinces = provinceDao.searchProvince(key);
		List<CityDto> cities = cityDao.searchCity(key);
		List<AreaDto> areas = areaDao.searchArea(key);
		LocationDto locations = new LocationDto(provinces, cities, areas);
		return locations;
	}

}
